export const SET_RESULTS_LOADING = 'RESULT_LOADING'
export const SET_RESULTS = 'SET_RESULTS'
export const SET_INPUT_VALUE = 'SET_INPUT_VALUE'

export const dispatchLoading = (loading = false) => ({
	type: SET_RESULTS_LOADING,
	loading
})

export const dispatchResults = (results) => ({
	type: SET_RESULTS,
	results
})

export const dispatchInputValue = (value = '') => ({
	type: SET_INPUT_VALUE,
	value
})
