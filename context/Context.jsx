import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import { SET_RESULTS, SET_RESULTS_LOADING, SET_INPUT_VALUE } from './actions'

export const Context = React.createContext()

const Provider = ({ children }) => {
	const initialState = {
		loading: false,
		results: {},
		inputValue: ''
	}

	const reducer = (state, action) => {
		switch (action.type) {
			case SET_RESULTS_LOADING:
				return {
					...state,
					loading: action.loading
				}

			case SET_RESULTS:
				return {
					...state,
					results: action.results
				}
			case SET_INPUT_VALUE:
				return {
					...state,
					inputValue: action.value
				}

			default:
				return state
		}
	}

	const [globalState, dispatch] = useReducer(reducer, initialState)

	return <Context.Provider value={{ globalState, dispatch }}>{children}</Context.Provider>
}

Provider.propTypes = {
	children: PropTypes.node.isRequired
}

export default Provider
