Number Converter

Application convert number into a list of corresponding words in the style 
of T9 [https://en.wikipedia.org/wiki/T9_(predictive_text)] 
or Phonewords [https://en.wikipedia.org/wiki/Phoneword]. 
For example, given the input 23 the output should be: ad, ae, af, bd, be, bf, cd, ce, cf.

After clone just run `npm ci` and `npm run dev`.