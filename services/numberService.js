import { appFetch } from './helpers/appFetch'

export const getNumberResults = (number) => {
	return appFetch(`/results/${number}`, 'GET')
}
