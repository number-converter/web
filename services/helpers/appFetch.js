import { API_URL } from '@/constants/apiConstants'
import handleError from './handleError'

export const appFetch = async (url, method, data = {}) => {
	const options = {
		method,
		headers: {
			'Content-type': 'application/json'
		}
	}

	if (data && Object.keys(data).length > 0) {
		options.body = JSON.stringify(data)
	}

	return fetch(`${API_URL}${url}`, options)
		.then(handleError)
		.then((res) => res.json())
}
