export default function handleError(response) {
	if (!response.ok) {
		return response.json().then((err) => Promise.reject(err))
	}
	return response
}
