const Differencify = require('differencify')
const rimraf = require('rimraf')
const getDifferencify = () => {
	return new Differencify({
		debug: true
	})
}

try {
	rimraf.sync('integration/__image_snapshots__/__current_output__')
	rimraf.sync('integration/__image_snapshots__/__differencified_output__')
} catch (error) {
	// eslint-disable-next-line no-console
	console.error('delete failed')
}

const TIMEOUT = 180000

const DEF_WIDTH_DESKTOP = 1280
const DEF_WIDTH_MOBILE = 320
const DEF_HEIGHT_MOBILE = 600
const DEF_HEIGHT_DESKTOP = 1200

let differencify

const BASE_URL = 'http://127.0.0.1:3000'

const tests = [
	{
		name: 'Homepage',
		url: `${BASE_URL}`
	},
	{
		name: 'History',
		url: `${BASE_URL}/history`
	},
	{
		name: 'SSR',
		url: `${BASE_URL}/ssr`
	},
	{
		name: 'SSRWithValue',
		url: `${BASE_URL}/ssr?value=23`
	}
]

describe('NumberConverter - desktop', () => {
	beforeAll(async () => {
		differencify = getDifferencify()
		await differencify.launchBrowser()
	})

	afterAll(async () => {
		await differencify.cleanup()
	})

	tests.forEach((options) => {
		const { name, width = DEF_WIDTH_DESKTOP, height = DEF_HEIGHT_DESKTOP, url } = options

		test(
			`Component ${name}`,
			async () => {
				const target = differencify.init({ testName: name, chain: false })
				const page = await target.newPage()

				await page.setViewport({ width, height })
				await page.goto(url)

				const image = await page.screenshot()
				const result = await target.toMatchSnapshot(image)

				await page.close()

				expect(result).toBe(true)
			},
			TIMEOUT
		)
	})
})

describe('NumberConverter - mobile.', () => {
	beforeAll(async () => {
		differencify = getDifferencify()
		await differencify.launchBrowser({
			defaultViewport: {
				isMobile: true,
				hasTouch: true,
				width: DEF_WIDTH_MOBILE,
				height: DEF_HEIGHT_MOBILE
			}
		})
	})

	afterAll(async () => {
		await differencify.cleanup()
	})

	tests.forEach((options) => {
		const { name, width = DEF_WIDTH_MOBILE, height = DEF_HEIGHT_MOBILE, url } = options

		test(
			`Component ${name} - mobile`,
			async () => {
				const target = differencify.init({ testName: `${name}-mobile`, chain: false })
				const page = await target.newPage()

				await page.setViewport({ width, height })
				await page.goto(url)

				const image = await page.screenshot()
				const result = await target.toMatchSnapshot(image)

				await page.close()

				expect(result).toBe(true)
			},
			TIMEOUT
		)
	})
})
