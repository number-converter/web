import 'jsdom-global/register'
import React from 'react'
import { mount, render, shallow } from 'enzyme'
import AppWrapper from './AppWrapper'

describe('AppWrapper', () => {
	it('Should have text Hello World', () => {
		const wrapper = shallow(
			<AppWrapper>
				<div>Hello World</div>
			</AppWrapper>
		)

		expect(wrapper.find('.children').text()).toEqual('Hello World')
	})

	it('Should have link to homepage', () => {
		const wrapper = mount(
			<AppWrapper>
				<div>Hello World</div>
			</AppWrapper>
		)

		expect(wrapper.find('li a').at(0).props().href).toEqual('/')

		wrapper.unmount()
	})
})

describe('AppWrapper - snapshots', () => {
	it('Should be displayed correctly', () => {
		const wrapper = render(
			<AppWrapper>
				<div>Hello</div>
			</AppWrapper>
		)

		expect(wrapper).toMatchSnapshot()
	})
})
