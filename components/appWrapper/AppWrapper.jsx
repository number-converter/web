import React from 'react'
import PropTypes from 'prop-types'
import NavigationBar from '@kiwicom/orbit-components/lib/NavigationBar'
import Mobile from '@kiwicom/orbit-components/lib/Mobile'
import Desktop from '@kiwicom/orbit-components/lib/Desktop'
import CSS_CLASSES from '@/constants/cssClasses'
import Stack from '@kiwicom/orbit-components/lib/Stack'
import TextLink from '@kiwicom/orbit-components/lib/TextLink'
import NewWindow from '@kiwicom/orbit-components/lib/icons/NewWindow'
import LinkList from '@kiwicom/orbit-components/lib/LinkList'
import { ROUTE_NAMES } from '@/constants/routeNames'
import { useRouter } from 'next/router'
import Footer from '@/components/footer/Footer'

import styles from './AppWrapper.module.scss'

const AppWrapper = ({ children }) => {
	return (
		<div>
			<NavigationBar>
				<Stack align='center' flex justify='between' spacing='none'>
					<div className={styles.logo}>
						<Mobile>
							<LogoLink text='NC' />
						</Mobile>
						<Desktop>
							<LogoLink text='Number Converter' />
						</Desktop>
					</div>
					<div>
						<LinkList direction='row'>
							<NavigationLink href={ROUTE_NAMES.HOMEPAGE} text='Home' />
							<NavigationLink href={ROUTE_NAMES.SSR} text='SSR' />
							<NavigationLink href={ROUTE_NAMES.HISTORY} text='History' />
							<TextLink
								iconRight={<NewWindow />}
								external={true}
								href='https://gitlab.com/number-converter'
								type='info'
							>
								repo
							</TextLink>
						</LinkList>
					</div>
				</Stack>
			</NavigationBar>
			<div className={styles.children}>{children}</div>
			<Footer />
		</div>
	)
}

AppWrapper.propTypes = {
	children: PropTypes.node.isRequired
}

const LogoLink = ({ text }) => {
	return (
		<TextLink href={ROUTE_NAMES.HOMEPAGE} type='primary' noUnderline={true}>
			<span className={CSS_CLASSES.BOLD}>{text}</span>
		</TextLink>
	)
}

LogoLink.propTypes = {
	text: PropTypes.string
}

const NavigationLink = ({ href, text }) => {
	const router = useRouter() || {}
	const linkType = router.pathname === href ? 'primary' : 'secondary'

	return (
		<TextLink href={href} type={linkType}>
			{text}
		</TextLink>
	)
}

NavigationLink.propTypes = {
	href: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired
}

export default AppWrapper
