import 'jsdom-global/register'
import React from 'react'
import { render } from 'enzyme'
import Keyboard from './Keyboard'

describe('Keyboard - snapshots', () => {
	it('Should be displayed correctly', () => {
		const keyboard = render(<Keyboard />)

		expect(keyboard).toMatchSnapshot()
	})
})
