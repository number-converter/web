import React, { useContext, useCallback, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import InputField from '@kiwicom/orbit-components/lib/InputField'
import Button from '@kiwicom/orbit-components/lib/Button'
import { getNumberResults } from '@/services/numberService'
import { Context } from '@/context/Context'
import { dispatchLoading, dispatchResults, dispatchInputValue } from '@/context/actions'
import { ERRORS } from '@/constants/errorConstants'
import Alert from '@kiwicom/orbit-components/lib/Alert'
import debounce from '@/helpers/debounce'
import { MAX_INPUT_LENGTH } from '@/constants/base'
import historyHandler from '@/helpers/historyHandler'
import ClearButton from '@/components/clearButton/ClearButton'

import styles from './Keyboard.module.scss'

const INPUT_NAME = 'numDebounce'

const Keyboard = () => {
	const { dispatch } = useContext(Context) || {}

	const [showAlert, setShowAlert] = useState(false)
	const [value, setValue] = useState('')
	const [hasError, setHasError] = useState(false)

	useEffect(() => {
		if (value) {
			handleInputChange(value)
		}
	}, [value])

	const handleInputChange = useCallback(
		debounce((value) => {
			setShowAlert(false)
			dispatch(dispatchLoading(true))

			getNumberResults(value)
				.then((data) => {
					const { results = {} } = data

					historyHandler(value)

					dispatch(dispatchLoading(false))
					dispatch(dispatchResults(results))
					dispatch(dispatchInputValue(value))
				})
				.catch((err) => {
					const { message = '' } = err

					if (message !== ERRORS.NOT_VALID) {
						setShowAlert(true)
					} else {
						setHasError(INPUT_NAME)
					}

					dispatch(dispatchLoading(false))
				})
		}, 1000),
		[]
	)

	const handleRemoveButton = () => {
		dispatch(dispatchResults({}))
		dispatch(dispatchInputValue())

		setValue('')
	}

	const handleValueChange = (val) => {
		setValue((prevState) => {
			if (prevState.length < MAX_INPUT_LENGTH) {
				return (prevState += val)
			} else {
				return prevState
			}
		})
	}

	return (
		<div className={styles.content}>
			{showAlert && (
				<div className={styles.alert}>
					<Alert icon title='Something went wrong' type='critical'>
						Try again!
					</Alert>
				</div>
			)}
			<InputField
				id='input_keyboard_field'
				error={hasError && 'Please enter valid number!'}
				label='Number'
				required={true}
				inputMode='tel'
				value={value}
				readOnly={true}
				help={`Maximum ${MAX_INPUT_LENGTH} numbers`}
				suffix={value && <ClearButton onClick={handleRemoveButton} />}
			/>
			<div className={styles.keyboard}>
				<KeyboardButton letters='' number={1} setValue={handleValueChange} />
				<KeyboardButton letters='ABC' number={2} setValue={handleValueChange} />
				<KeyboardButton letters='DEF' number={3} setValue={handleValueChange} />
				<KeyboardButton letters='GHI' number={4} setValue={handleValueChange} />
				<KeyboardButton letters='JKL' number={5} setValue={handleValueChange} />
				<KeyboardButton letters='MNO' number={6} setValue={handleValueChange} />
				<KeyboardButton letters='PQRS' number={7} setValue={handleValueChange} />
				<KeyboardButton letters='TUV' number={8} setValue={handleValueChange} />
				<KeyboardButton letters='WXYZ' number={9} setValue={handleValueChange} />
				<KeyboardButton letters='*' />
				<KeyboardButton letters='' number={0} />
				<KeyboardButton letters='#' />
			</div>
		</div>
	)
}

const KeyboardButton = ({ letters, number, setValue }) => {
	return (
		<Button
			asComponent={ButtonComp}
			type='primarySubtle'
			onClick={() => setValue && setValue(number)}
		>
			<div className={styles.btnWrapInner}>
				{number && <span>{number}</span>}
				<span>{letters}</span>
			</div>
		</Button>
	)
}

KeyboardButton.propTypes = {
	letters: PropTypes.string,
	number: PropTypes.number,
	setValue: PropTypes.func
}

const ButtonComp = (props) => {
	const { className = '' } = props

	const classNameValue = className + ` ${styles.btnWrap}`

	return <div {...props} className={classNameValue} />
}

ButtonComp.propTypes = {
	className: PropTypes.string
}

export default Keyboard
