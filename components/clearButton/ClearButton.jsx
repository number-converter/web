import React from 'react'
import PropTypes from 'prop-types'
import ButtonLink from '@kiwicom/orbit-components/lib/ButtonLink'
import CloseIcon from '@kiwicom/orbit-components/lib/icons/Close'
import E2E from '@/constants/e2eConstants'

const ClearButton = ({ onClick }) => {
	return (
		<ButtonLink
			dataTest={E2E.CLEAR_BUTTON}
			compact
			iconLeft={<CloseIcon />}
			onClick={onClick}
			type='primary'
		/>
	)
}

ClearButton.propTypes = {
	onClick: PropTypes.func.isRequired
}

export default ClearButton
