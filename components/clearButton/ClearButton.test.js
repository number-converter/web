import 'jsdom-global/register'
import React from 'react'
import { render } from 'enzyme'
import ClearButton from './ClearButton'

describe('ClearButton - snapshots', () => {
	it('Should be displayed correctly', () => {
		const button = render(<ClearButton onClick={() => {}} />)

		expect(button).toMatchSnapshot()
	})
})
