import React from 'react'
import { shallow } from 'enzyme'
import Footer from './Footer'

describe('Footer', () => {
	const footer = shallow(<Footer />)
	it('Should have correct text', () => {
		expect(footer.text()).toEqual('Made with by Mirec')
	})

	it('Should contain img tag', () => {
		const img = footer.find('img')
		expect(img.length).toBe(1)
	})
})

describe('Footer - snapshots', () => {
	it('should render correctly', () => {
		const footer = shallow(<Footer />)

		expect(footer).toMatchSnapshot()
	})
})
