import React from 'react'

import styles from './Footer.module.scss'

const Footer = () => {
	return (
		<div className={styles.content}>
			<div className={styles.text}>
				Made with <img className={styles.heart} src='/heart.png' />
				by Mirec
			</div>
		</div>
	)
}

export default Footer
