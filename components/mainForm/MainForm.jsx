import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import InputField from '@kiwicom/orbit-components/lib/InputField'
import Button from '@kiwicom/orbit-components/lib/Button'
import { useForm, Controller } from 'react-hook-form'
import { useRouter } from 'next/router'
import { ROUTE_NAMES } from '@/constants/routeNames'
import { URL_PARAMS } from '@/constants/urlConvertor'
import { ERRORS } from '@/constants/errorConstants'
import Alert from '@kiwicom/orbit-components/lib/Alert'
import { MAX_INPUT_LENGTH } from '@/constants/base'
import E2E from '@/constants/e2eConstants'
import ClearButton from '@/components/clearButton/ClearButton'

import styles from './MainForm.module.scss'

const INPUT_NAME = 'num'

const MainForm = ({ defaultValue = '', hasError = false, errorValue = {} }) => {
	const [showAlertState, setShowAlertState] = useState(false)

	const handleError = (errorObj = {}) => {
		const { message = '' } = errorObj

		if (message !== ERRORS.NOT_VALID) {
			setShowAlertState(true)
		} else {
			setError(INPUT_NAME)
		}
	}

	useEffect(() => {
		setValue(INPUT_NAME, defaultValue)
	}, [defaultValue])

	useEffect(() => {
		if (hasError) {
			handleError(errorValue)
		}
	}, [hasError])

	const router = useRouter()

	const {
		handleSubmit,
		formState: { errors },
		setError,
		control,
		setValue
	} = useForm()

	const onSubmit = (data) => {
		const { [INPUT_NAME]: value } = data

		router.push(`${ROUTE_NAMES.SSR}?${URL_PARAMS.APP.VALUE}=${value}`)
	}

	const handleRemoveButton = () => {
		router.push(ROUTE_NAMES.SSR)

		setValue(INPUT_NAME, '')
	}

	return (
		<div className={styles.content}>
			{showAlertState && (
				<div className={styles.alert}>
					<Alert icon title='Something went wrong' type='critical'>
						Try again!
					</Alert>
				</div>
			)}
			<form onSubmit={handleSubmit(onSubmit)}>
				<Controller
					name={INPUT_NAME}
					control={control}
					defaultValue=''
					rules={{
						required: true,
						validate: (val) => {
							const numberValue = Number(val)

							return !isNaN(numberValue) ?? false
						}
					}}
					render={({ field }) => (
						<InputField
							id='input_field'
							error={!!errors[INPUT_NAME] && 'Please enter valid number!'}
							label='Number'
							required={true}
							inputMode='tel'
							maxLength={MAX_INPUT_LENGTH}
							suffix={field.value && <ClearButton onClick={handleRemoveButton} />}
							{...field}
						/>
					)}
				/>
				<div className={styles.button}>
					<Button dataTest={E2E.SUBMIT} fullWidth={true} submit={true}>
						Submit
					</Button>
				</div>
			</form>
		</div>
	)
}

MainForm.propTypes = {
	defaultValue: PropTypes.string,
	hasError: PropTypes.bool,
	errorValue: PropTypes.object
}

export default MainForm
