import 'jsdom-global/register'
import React from 'react'
import { mount, render } from 'enzyme'
import MainForm from './MainForm'

describe('MainForm', () => {
	it('Should have value 23', () => {
		const form = mount(<MainForm />)

		const input = form.find('form input')
		input.props().value = '23'

		expect(input.props().value).toEqual('23')

		form.unmount()
	})
})

describe('MainForm - snapshots', () => {
	it('Should be displayed correctly', () => {
		const form = render(<MainForm />)

		expect(form).toMatchSnapshot()
	})
})
