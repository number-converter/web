import 'jsdom-global/register'
import React from 'react'
import { mount, shallow, render } from 'enzyme'
import Results from './Results'

const resultsValue = {
	values: ['hi', 'joy', 'test'],
	count: 3
}

describe('Results', () => {
	it('Should have correct no results text', () => {
		const results = render(<Results loading={false} inputValue='123' />)
		expect(results.text()).toEqual('No results for value: "123"')
	})

	it('Should have correct count and values', () => {
		const results = render(<Results loading={false} inputValue='123' results={resultsValue} />)
		expect(results.find('.count').text()).toEqual('Count: 3')

		expect(results.find('ul > li').length).toEqual(3)
	})
})

describe('Results - snapshots', () => {
	it('should render correctly placeholders', () => {
		const results = mount(<Results loading={true} />)

		expect(results).toMatchSnapshot()

		results.unmount()
	})

	it('should render correctly no results', () => {
		const results = shallow(<Results loading={false} inputValue='123' />)

		expect(results).toMatchSnapshot()
	})

	it('should render correctly results', () => {
		const results = render(<Results loading={false} inputValue='123' results={resultsValue} />)

		expect(results).toMatchSnapshot()
	})
})
