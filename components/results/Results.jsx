import React from 'react'
import PropTypes from 'prop-types'
import List, { ListItem } from '@kiwicom/orbit-components/lib/List'
import CSS_CLASSES from '@/constants/cssClasses'
import Illustration from '@kiwicom/orbit-components/lib/Illustration'
import E2E from '@/constants/e2eConstants'

import styles from './Results.module.scss'

const Results = (props) => {
	const { loading, results = {}, inputValue = '' } = props

	const { values = [], count } = results

	if (loading) {
		return (
			<WrapperComponent>
				<PlaceholderComponent />
			</WrapperComponent>
		)
	} else {
		return (
			<WrapperComponent>
				{values.length > 0 ? (
					<div>
						<div className={styles.count}>
							Count: <span className={CSS_CLASSES.BOLD}>{count}</span>
						</div>
						<div className={styles.list}>
							<List size='normal' type='primary'>
								{values.map((value, index) => (
									<ListItem
										dataTest={`${E2E.RESULT_VALUE}__${value}`}
										key={`${index}_value_${value}`}
									>
										{value}
									</ListItem>
								))}
							</List>
						</div>
					</div>
				) : (
					inputValue && (
						<div className={styles.noResults}>
							<Illustration name='NoResults' size='medium' />
							<div data-test={E2E.NO_RESULT} className={styles.noResultsText}>
								No results for value:{' '}
								<span className={CSS_CLASSES.BOLD}>&quot;{inputValue}&quot;</span>
							</div>
						</div>
					)
				)}
			</WrapperComponent>
		)
	}
}

const WrapperComponent = ({ children }) => {
	return <div className={styles.content}>{children}</div>
}

WrapperComponent.propTypes = {
	children: PropTypes.node.isRequired
}

const PlaceholderComponent = () => {
	const placeholderArray = Array.from(Array(12).keys())

	return (
		<div className={styles.placeholders}>
			{placeholderArray.map((placeholder) => (
				<div key={`${placeholder}__key`} className={styles.placeholder}></div>
			))}
		</div>
	)
}

export default Results
