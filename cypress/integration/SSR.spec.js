/// <reference types="Cypress" />
import E2E from '../../constants/e2eConstants'

describe('SSR tests', () => {
	it('Input should contain correct value', () => {
		cy.visit('/ssr?value=23')

		cy.get('input').should('have.value', '23')
	})

	it('Should display correct results', () => {
		cy.visit('/ssr?value=5494')
		cy.get('input').should('have.value', '5494')
		cy.get(`[data-test="${E2E.RESULT_VALUE}__kiwi"]`).should('have.text', 'kiwi')
	})

	it('Should display correct results with submit', () => {
		cy.visit('/ssr')

		cy.get('input').type('23')
		cy.get(`button[data-test="${E2E.SUBMIT}"]`).click()

		cy.url().should('eq', Cypress.config().baseUrl + '/ssr?value=23')
		cy.get(`[data-test="${E2E.RESULT_VALUE}__be"]`).should('have.text', 'be')
	})

	it('Should clear input value', () => {
		cy.visit('/ssr')

		cy.get('input').type('2332')
		cy.get(`button[data-test="${E2E.SUBMIT}"]`).click()

		cy.url().should('eq', Cypress.config().baseUrl + '/ssr?value=2332')

		cy.get(`[data-test="${E2E.CLEAR_BUTTON}"]`).click()

		cy.url().should('eq', Cypress.config().baseUrl + '/ssr')
	})

	it('Should display no results', () => {
		cy.visit('/ssr?value=5555')

		cy.get('input').should('have.value', '5555')
		cy.get(`[data-test="${E2E.NO_RESULT}"`).should('exist')
	})

	it('Should display error message', () => {
		cy.visit('/ssr?value=err')

		cy.get('input').should('have.value', 'err')
		cy.get('div').contains('Please enter valid number!').should('exist')
	})
})
