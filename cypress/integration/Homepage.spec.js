/// <reference types="Cypress" />
import E2E from '../../constants/e2eConstants'

describe('Homepage tests', () => {
	it('Input should contain correct value', () => {
		cy.visit('/')

		cy.get('div[type="button"]').contains('2').click()
		cy.get('div[type="button"]').contains('3').click()

		cy.get('input').should('have.value', '23')
	})

	it('Should display correct results', () => {
		cy.intercept('GET', '**/api/v1/results/*').as('results')
		cy.visit('/')

		cy.get('div[type="button"]').contains('5').click()
		cy.get('div[type="button"]').contains('4').click()
		cy.get('div[type="button"]').contains('9').click()
		cy.get('div[type="button"]').contains('4').click()

		cy.wait('@results')

		cy.get('input').should('have.value', '5494')
		cy.get(`[data-test="${E2E.RESULT_VALUE}__kiwi"]`).should('have.text', 'kiwi')
	})

	it('Should clear results and input value', () => {
		cy.intercept('GET', '**/api/v1/results/*').as('results')
		cy.visit('/')

		cy.get('div[type="button"]').contains('5').click()
		cy.get('div[type="button"]').contains('4').click()
		cy.get('div[type="button"]').contains('9').click()
		cy.get('div[type="button"]').contains('4').click()

		cy.wait('@results')

		cy.get('input').should('have.value', '5494')

		cy.get(`[data-test="${E2E.CLEAR_BUTTON}"]`).click()
		cy.get('input').should('have.value', '')
	})

	it('Should display no results', () => {
		cy.intercept('GET', '**/api/v1/results/*').as('results')
		cy.visit('/')

		cy.get('div[type="button"]').contains('5').click()
		cy.get('div[type="button"]').contains('5').click()
		cy.get('div[type="button"]').contains('5').click()
		cy.get('div[type="button"]').contains('5').click()
		cy.wait('@results')

		cy.get('input').should('have.value', '5555')
		cy.get(`[data-test="${E2E.NO_RESULT}"`).should('exist')
	})
})
