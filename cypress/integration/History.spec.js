/// <reference types="Cypress" />
import E2E from '../../constants/e2eConstants'

describe('History tests', () => {
	it('Should render page history', () => {
		cy.visit('/history')

		cy.get(`[data-test="${E2E.HISTORY_TITLE}"]`).should('have.text', 'History')
	})

	it('Should have no results by default', () => {
		cy.visit('/history')

		cy.get(`[data-test="${E2E.NO_RESULT}"]`).should('exist')
	})

	it('Should add value to history', () => {
		cy.visit('/ssr?value=23')
		cy.visit('/history')

		cy.get(`[data-test="${E2E.NO_RESULT}"]`).should('not.exist')
		cy.get(`[data-test="${E2E.HISTORY_VALUE}__23"]`).should('exist')
	})

	it('Should remove value from history', () => {
		cy.visit('/ssr?value=23')
		cy.visit('/history')

		cy.get(`[data-test="${E2E.NO_RESULT}"]`).should('not.exist')
		cy.get(`[data-test="${E2E.HISTORY_VALUE}__23"]`).should('exist')

		cy.get(`[data-test="${E2E.HISTORY_REMOVE}"]`).click()

		cy.get(`[data-test="${E2E.NO_RESULT}"]`).should('exist')
		cy.get(`[data-test="${E2E.HISTORY_VALUE}__23"]`).should('not.exist')
	})

	it('Should display detail', () => {
		cy.intercept('GET', '**/api/v1/results/*').as('results')
		cy.visit('/ssr?value=23')
		cy.visit('/history')

		cy.get(`[data-test="${E2E.NO_RESULT}"]`).should('not.exist')
		cy.get(`[data-test="${E2E.HISTORY_VALUE}__23"]`).should('exist')

		cy.get(`[data-test="${E2E.HISTORY_DETAIL}"]`).click()
		cy.wait('@results')

		cy.get(`[data-test="${E2E.RESULT_VALUE}__be"]`).should('have.text', 'be')
	})
})
