module.exports = {
	testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
	setupFilesAfterEnv: ['<rootDir>/jestSetupFile.js'],
	transform: {
		'^.+\\.(js|jsx)$': '<rootDir>/node_modules/babel-jest'
	},
	moduleNameMapper: {
		'\\.(css|scss)$': 'identity-obj-proxy',
		'^@/constants(.*)$': '<rootDir>/constants$1',
		'^@/services(.*)$': '<rootDir>/services$1',
		'^@/helpers(.*)$': '<rootDir>/helpers$1',
		'^@/context(.*)$': '<rootDir>/context$1',
		'^@/components(.*)$': '<rootDir>/components$1'
	},
	snapshotSerializers: ['enzyme-to-json/serializer'],
	testMatch: ['**.test.js']
}
