module.exports = () => {
	return {
		async rewrites() {
			return [
				{
					source: '/api/v1/:slug*',
					destination: `http://127.0.0.1:5000/api/v1/:slug*`,
					basePath: false
				}
			]
		},
		eslint: {
			ignoreDuringBuilds: true
		}
	}
}
