import { LOCAL_STORAGE_NAME } from '@/constants/base'

export default function historyHandler(value) {
	if (value) {
		const getLocalStorageHistory = localStorage.getItem(LOCAL_STORAGE_NAME)

		const newValue = {
			value,
			date: new Date(),
			id: new Date().valueOf().toString()
		}

		if (getLocalStorageHistory !== null) {
			const newValues = JSON.parse(getLocalStorageHistory).concat([newValue])

			localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(newValues))
		} else {
			localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify([newValue]))
		}
	}
}
