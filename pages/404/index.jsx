import React from 'react'
import { ROUTE_NAMES } from '@/constants/routeNames'
import Button from '@kiwicom/orbit-components/lib/Button'
import Illustration from '@kiwicom/orbit-components/lib/Illustration'

import styles from './PageNotFound.module.scss'

const PageNotFound = () => {
	return (
		<div className={styles.content}>
			<div className={styles.wrapper}>
				<Illustration name='Error404' size='large' />
				<div className={styles.info}>Page not found</div>
				<Button type='primary' size='normal' href={ROUTE_NAMES.HOMEPAGE}>
					Continue to Homepage
				</Button>
			</div>
		</div>
	)
}

export default PageNotFound
