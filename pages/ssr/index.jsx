import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import MainForm from '@/components/mainForm/MainForm'
import Results from '@/components/results/Results'
import Heading from '@kiwicom/orbit-components/lib/Heading'
import { useRouter } from 'next/router'
import { URL_PARAMS } from '@/constants/urlConvertor'
import { getNumberResults } from '@/services/numberService'
import historyHandler from '@/helpers/historyHandler'
import Head from 'next/head'

import styles from './Query.module.scss'

const HomePage = ({ results = {}, dataLoaded = false, hasError = false, errorValue }) => {
	const router = useRouter()
	const { [URL_PARAMS.APP.VALUE]: numberValue = '' } = router.query

	useEffect(() => {
		historyHandler(numberValue)
	}, [numberValue])

	return (
		<React.Fragment>
			<Head>
				<title>SSR example</title>
				<meta name='description' content='This example using server side rendering.' />
			</Head>
			<div className={styles.content}>
				<Heading spaceAfter='normal'>SSR example</Heading>
				<MainForm hasError={hasError} defaultValue={numberValue} errorValue={errorValue} />
				<Results loading={!dataLoaded} results={results} inputValue={hasError ? '' : numberValue} />
			</div>
		</React.Fragment>
	)
}

HomePage.propTypes = {
	results: PropTypes.object,
	dataLoaded: PropTypes.bool,
	hasError: PropTypes.bool,
	errorValue: PropTypes.object
}

export async function getServerSideProps(context) {
	const { [URL_PARAMS.APP.VALUE]: numberValue = '' } = context.query || {}

	if (numberValue) {
		try {
			const { results = {} } = (await getNumberResults(numberValue)) || {}

			return {
				props: {
					results,
					dataLoaded: true
				}
			}
		} catch (error) {
			return {
				props: {
					results: {},
					dataLoaded: true,
					hasError: true,
					errorValue: error.statusCode ? error : {}
				}
			}
		}
	} else {
		return {
			props: {
				results: {},
				dataLoaded: true,
				hasError: false
			}
		}
	}
}

export default HomePage
