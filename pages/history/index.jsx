import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import Heading from '@kiwicom/orbit-components/lib/Heading'
import { LOCAL_STORAGE_NAME } from '@/constants/base'
import List from '@kiwicom/orbit-components/lib/List'
import Text from '@kiwicom/orbit-components/lib/Text'
import Button from '@kiwicom/orbit-components/lib/Button'
import Calendar from '@kiwicom/orbit-components/lib/icons/Calendar'
import RemoveIcon from '@kiwicom/orbit-components/lib/icons/Remove'
import CSS_CLASSES from '@/constants/cssClasses'
import Modal, { ModalHeader, ModalSection } from '@kiwicom/orbit-components/lib/Modal'
import Results from '@/components/results/Results'
import { getNumberResults } from '@/services/numberService'
import Illustration from '@kiwicom/orbit-components/lib/Illustration'
import E2E from '@/constants/e2eConstants'
import Head from 'next/head'

import styles from './History.module.scss'

const History = () => {
	const [values, setValues] = useState([])
	const [openId, setOpenId] = useState('')

	useEffect(() => {
		const historyValues = localStorage.getItem(LOCAL_STORAGE_NAME)

		const parsedValues = historyValues ? JSON.parse(historyValues).sort((a, b) => b.id - a.id) : []

		setValues(parsedValues)
	}, [])

	const handleRemove = (id) => {
		const newLocalStoraValues = values.filter((historyValue) => historyValue.id !== id)

		localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(newLocalStoraValues))
		setValues(newLocalStoraValues)
	}

	return (
		<React.Fragment>
			<Head>
				<title>History</title>
				<meta name='description' content='This is history.' />
			</Head>
			<div className={styles.content}>
				<Heading dataTest={E2E.HISTORY_TITLE} spaceAfter='normal'>
					History
				</Heading>
				{values.length > 0 ? (
					<div className={styles.list}>
						<List size='normal' type='primary'>
							{values.map((historyValue, index) => {
								const { value, date, id } = historyValue

								const dateValue = new Date(date)
								const day = dateValue.getDate()
								const month = dateValue.getMonth()
								const year = dateValue.getFullYear()

								return (
									<li
										data-test={`${E2E.HISTORY_VALUE}__${value}`}
										key={`${index}_history_${id}`}
										className={styles.item}
									>
										<div className={styles.valueWrap}>
											<div className={CSS_CLASSES.BOLD}>{value}</div>
											<div className={styles.info}>
												<div className={styles.dateWrap}>
													<div className={styles.date}>{`${day}/${month}/${year}`}</div>
													<Calendar color='secondary' size='small' />
												</div>
												<div className={styles.actions}>
													<Button
														dataTest={E2E.HISTORY_DETAIL}
														size='small'
														onClick={() => setOpenId(id)}
													>
														Detail
													</Button>
													{openId === id && <ModalComponent value={value} setOpenId={setOpenId} />}
													<div className={styles.removeBtn}>
														<Button
															size='small'
															type='criticalSubtle'
															circled={true}
															iconLeft={<RemoveIcon />}
															onClick={() => handleRemove(id)}
															dataTest={E2E.HISTORY_REMOVE}
														/>
													</div>
												</div>
											</div>
										</div>
									</li>
								)
							})}
						</List>
					</div>
				) : (
					<div className={styles.noResults} data-test={E2E.NO_RESULT}>
						<Illustration name='NoResults' size='medium' />
						<Text align='center' type='secondary'>
							No history values
						</Text>
					</div>
				)}
			</div>
		</React.Fragment>
	)
}

const ModalComponent = ({ value, setOpenId }) => {
	const [loading, setLoading] = useState(true)
	const [resultValues, setResultValues] = useState([])

	useEffect(() => {
		getNumberResults(value)
			.then((data) => {
				const { results = {} } = data

				setLoading(false)
				setResultValues(results)
			})
			.catch((err) => {
				setLoading(false)
			})
	}, [])

	return (
		<Modal mobileHeader={true} onClose={() => setOpenId('')}>
			<ModalHeader title={`Results for ${value}`} />
			<ModalSection>
				<Results loading={loading} results={resultValues} inputValue={value} />
			</ModalSection>
		</Modal>
	)
}

ModalComponent.propTypes = {
	value: PropTypes.string,
	setOpenId: PropTypes.func
}

export default History
