import React from 'react'
import PropTypes from 'prop-types'
import AppWrapper from '@/components/appWrapper/AppWrapper'
import Provider from '@/context/Context'
import Head from 'next/head'

import 'normalize.css'
import '@/styles/globals.scss'

const MyApp = ({ Component, pageProps }) => {
	return (
		<React.Fragment>
			<Head>
				<title>Number Converter</title>
				<meta name='description' content='Number Converter application.' />
			</Head>
			<Provider>
				<AppWrapper>
					<Component {...pageProps} />
				</AppWrapper>
			</Provider>
		</React.Fragment>
	)
}

MyApp.propTypes = {
	Component: PropTypes.elementType,
	pageProps: PropTypes.object
}

export default MyApp
