import React, { useContext } from 'react'
import Results from '@/components/results/Results'
import { Context } from '@/context/Context'
import Heading from '@kiwicom/orbit-components/lib/Heading'
import Keyboard from '@/components/keyboard/Keyboard'
import Head from 'next/head'

import styles from './HomePage.module.scss'

const HomePage = () => {
	const { globalState = {} } = useContext(Context)
	const { loading, results, inputValue } = globalState

	return (
		<React.Fragment>
			<Head>
				<title>Keyboard UI example</title>
				<meta name='description' content='This example using mobile keyboard.' />
			</Head>
			<div className={styles.content}>
				<Heading spaceAfter='normal'>Keyboard UI example</Heading>
				<Keyboard />
				<Results loading={loading} results={results} inputValue={inputValue} />
			</div>
		</React.Fragment>
	)
}

export default HomePage
