const E2E = {
	NO_RESULT: 'e2e-no-res',
	RESULT_VALUE: 'e2e-result-value',
	SUBMIT: 'e2e-submit',
	CLEAR_BUTTON: 'e2e-clear',
	HISTORY_TITLE: 'e2e-history-title',
	HISTORY_VALUE: 'e2e-history-value',
	HISTORY_REMOVE: 'e2e-history-remove',
	HISTORY_DETAIL: 'e2e-history-detail'
}

export default E2E
